from PyQt5.QtWidgets import (QApplication, QMainWindow)

from listeners.Listener_Shape import Listener_Shape
from shape.Rectangle import rectangle
from shape.Square import square

shape_list = []


class MainWindow(QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()
        self.setWindowTitle("Tetris | alpha 0.1")
        self.setGeometry(0, 0, 1000, 800)

        shape_list.append(self.createShape(square, 0, "blue"))
        shape_list.append(self.createShape(rectangle, 100, "yellow"))
        shape_list.append(self.createShape(rectangle, 200, "red"))

        Listener_Shape()

    def paintEvent(self, e):
        for i in shape_list:
            i.painter()

    def createShape(self, type, y, color):
        result = type(self)
        result.setY(y)
        result.setColor(color)
        return result


if __name__ == '__main__':
    import sys

    app = QApplication(sys.argv)
    win = MainWindow()
    win.show()
    sys.exit(app.exec_())

    # TODO: lLetterL, lLetterR, stairL, stairR, tLetter
