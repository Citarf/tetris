from PyQt5.QtGui import (QColor)


class Shape:
    def __init__(self, canvas):
        self.cvs = canvas
        self.cvsWidth = canvas.width
        self.x = None
        self.y = None
        self.o = None
        self.a = False
        self.width = 20
        self.height = 20
        self.speed = 1.0
        self.color = QColor(0, 0, 200)

    def setWidht(self, value):
        """
        Set the shape's width.
        :param value: between 1 and 4.
        :return:
        """
        if (value <= 4 or value > 0):
            self.width = 10 * value
        else:
            raise ValueError("Width Shape Error: Your shape can't be wider than 4.")

    def setHeight(self, value):
        """
        Set the shape's width.
        :param value: between 1 and 4.
        :return:
        """
        if (value <= 4 or value > 0):
            print("setHeight")
            self.height = 10 * value
        else:
            raise ValueError("Height Shape Error: Your shape can't be taller than 4.")

    def setY(self, value):
        if (type(value) == int or type(value) == float):
            self.y = value
        else:
            raise ValueError("Value must be an integer or a float.")

    def setColor(self, value):
        if (value == "red"):
            self.color = QColor(200, 0, 0)
        elif (value == "blue"):
            self.color = QColor(0, 0, 200)
        elif (value == "green"):
            self.color = QColor(0, 200, 0)
        elif (value == "yellow"):
            self.color = QColor(200, 200, 0)
        else:
            raise ValueError("Choose correct color: red, blue, green, yellow.")
