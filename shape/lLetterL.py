from PyQt5.QtGui import (QPainter)

from shape.Shape import Shape


class lLetterL(Shape):
    def __init__(self, canvas):
        super().__init__(canvas)
        self.setWidht(3)
        self.setHeight(1)
        self.x = canvas.width() / 2 - 10
        self.y = 0

    def painter(self):
        painter = QPainter(self.cvs)
        painter.fillRect(self.x, self.y, self.width, self.height, self.color)
