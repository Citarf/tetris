from PyQt5.QtGui import (QPainter)

from shape.Shape import Shape


class rectangle(Shape):
    def __init__(self, canvas):
        super().__init__(canvas)
        self.setWidht(4)
        self.setHeight(1)
        self.x = canvas.width() / 2 - 20
        self.y = 100

    def painter(self):
        painter = QPainter(self.cvs)
        painter.fillRect(self.x, self.y, self.width, self.height, self.color)
