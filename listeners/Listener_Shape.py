from PyQt5.QtCore import (Qt)
from PyQt5.QtWidgets import (QMainWindow)


class Listener_Shape(QMainWindow):
    def __init__(self):
        super().__init__()

    def keyPressEvent(self, QKeyEvent):
        code = QKeyEvent.key()
        if (code == Qt.Key_Up):
            print('Up')
        if (code == Qt.Key_Down):
            print('Down')
        if (code == Qt.Key_F):
            print('F')
